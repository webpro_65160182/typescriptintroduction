function printStatusCode(code: string|number) {
    if (typeof code ==='string') {
        console.log(`My status code is ${code.toUpperCase()} ${typeof code}`); // `` ตัวนี้กดที่ปุ่มเปลี่ยนภาษาWindow + ปุ่มเปลี่ยนภาษา(เป็นอังกฤษ)
    } else {
        console.log(`My status code is ${code} ${typeof code}`); // `` ตัวนี้กดที่ปุ่มเปลี่ยนภาษาWindow + ปุ่มเปลี่ยนภาษา(เป็นอังกฤษ)
    }
}

printStatusCode(404);
printStatusCode("abc");