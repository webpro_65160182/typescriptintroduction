const names: string[] = [];
// const names: readonly string[] = []; //พอใส่readonlyทำให้เพิ่มค่าเข้าไม่ได้
names.push("Dylan");
names.push("Pitchayapon");
// names.push(3);  //Errorเพราะมีค่ากำหนดTypeไว้อยู่แล้ว
console.log(names[0]);
console.log(names[1]);
console.log(names.length); //ความยาวของArray

for(let i =0;i<names.length;i++) {
    console.log(names[i]);  //แสดงข้อมูลของArrayทีละตัวแบบ 1
}

for(let ind in names) {
    console.log(names[ind]);  //แสดงข้อมูลของArrayทีละตัวแบบ 2
}

names.forEach(function (name) {
    console.log(name)  //แสดงข้อมูลของArrayทีละตัวแบบ 3
})