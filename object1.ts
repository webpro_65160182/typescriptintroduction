// const car: {type: string, model : string, year : number} = {
//     type: "Toyota",
//     model: "Corolla",
//     year: 2009 //ถ้าไม่ใส่จะErrorเพราะมีTypeให้แล้ว
// }; //สร้างObjขึ้นมาแต่ต้องกำหนดTypeด้วย

// console.log(car);// ถ้าไม่ใช้console.logและไม่กำหนดTypeในObjจะRunผ่าน

const car: {type: string, model : string, year? : number} = {
    type: "Toyota",
    model: "Corolla"//ถ้าไม่ใส่yearจะไม่Errorเพราะใส่?หลังyearแล้ว
};

console.log(car);