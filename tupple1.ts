let ourTuple: readonly [number, boolean, string]; //กำหนดType
ourTuple = [5, false, "Coding God was here"]; //สลับตำแหน่งไม่ได้
// ourTuple.push("Something new and wrong"); //ควรตั้งreadonlyไม่งั้นpushได้
console.log(ourTuple);